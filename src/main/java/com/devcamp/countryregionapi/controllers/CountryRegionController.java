package com.devcamp.countryregionapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.model.Country;
import com.devcamp.countryregionapi.model.Region;
import com.devcamp.countryregionapi.service.CountryService;
import com.devcamp.countryregionapi.service.RegionService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CountryRegionController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> getCountriesApi(){
        return countryService.listCountries();
    }
    
    @GetMapping("/country-info")
    public Country getCountryInfoApi(@RequestParam(value="q", defaultValue = "") String countryCode){
        Country country = null;
        for (int i = 0; i < countryService.listCountries().size(); i++){
            if (countryService.listCountries().get(i).getCountryCode().equals(countryCode)){
                country = countryService.listCountries().get(i);
            }
        }
        return country;
    }
    @Autowired
    private RegionService regionService;
    @GetMapping("/region-info")
    public Region getRegionInfo(@RequestParam(value="q", defaultValue = "") String regionCode){
        Region region = null;
        for (int i=0; i< regionService.allRegionList().size(); i++){
            if (regionService.allRegionList().get(i).getRegionCode().equals(regionCode)){
                region = regionService.allRegionList().get(i);
            }
        }
        return region;
    }
}
