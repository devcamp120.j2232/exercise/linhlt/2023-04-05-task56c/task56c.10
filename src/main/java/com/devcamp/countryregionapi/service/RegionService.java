package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;
@Service
public class RegionService {
    Region region1 = new Region("HAN","Hanoi");
    Region region2 = new Region("HCM","Hochiminh");
    Region region3 = new Region("CALI","California");
    Region region4 = new Region("WAH","Washington");
    Region region5 = new Region("MEL","Melbourn");
    Region region6 = new Region("SYD","Sydney");
    public ArrayList<Region> listRegionsVN(){
        ArrayList<Region> regionsVN = new ArrayList<>();
        regionsVN.add(region1);
        regionsVN.add(region2);
        return regionsVN;
    }
    public ArrayList<Region> listRegionsUSA(){
        ArrayList<Region> regionsUSA = new ArrayList<>();
        regionsUSA.add(region3);
        regionsUSA.add(region4);
        return regionsUSA;
    }
    public ArrayList<Region> listRegionsAUS(){
        ArrayList<Region> regionsAUS = new ArrayList<>();
        regionsAUS.add(region5);
        regionsAUS.add(region6);
        return regionsAUS;
    }
    public ArrayList<Region> allRegionList(){
        ArrayList<Region> regionList = new ArrayList<>();
        regionList.add(region1);
        regionList.add(region2);
        regionList.add(region3);
        regionList.add(region4);
        regionList.add(region5);
        regionList.add(region6);
        return regionList;
    }
}
