package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Country;
@Service
public class CountryService extends RegionService{
    public ArrayList<Country> listCountries(){
        Country country1 = new Country("VN", "Vietnam", listRegionsVN());
        Country country2 = new Country("USA","America", listRegionsUSA());
        Country country3 = new Country("AUS","Australia", listRegionsAUS());
        ArrayList<Country> countries = new ArrayList<>();
        countries.add(country1);
        countries.add(country2);
        countries.add(country3);
        return countries;
    }

}
